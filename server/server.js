require("./config/config");
require("./config/connectionDB");

const express = require("express");

const app = express();

const bodyParser = require("body-parser");

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.use(require("./controllers/tasksController").app);

app.listen(process.env.PORT, () =>
    console.log(`Escuchando el puerto ${process.env.PORT}`)
);